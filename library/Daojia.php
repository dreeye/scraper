<?php
require 'vendor/autoload.php';
use Goutte\Client;

class Daojia
{

    private $menu;
    private $client;
    private $crawler;

    public function __construct()
    {
        $this->client = new Client();
        $this->crawler = $this->client->request('GET', 'http://beijing.daojia.com.cn/area/1/'); 
        # 菜谱uri
        # $uri = $link->getUri();

    }

    public function menu()
    {
        $link = $this->crawler->selectLink('云海肴云南菜')->link();
        $menu = $this->client->click($link);
        $menu->filter(".hottest_dishes")->filter("tr")->each(function ($node, $i) {

            $img = $node->filter('a')->attr('onmouseover');
            if (preg_match("/'http:\/\/.*?'/i", $img, $matches)) {
                $this->menu[$i]['img'] = $matches[0];
            }
            else
            {
                $this->menu[$i]['img'] = '';

            }
            $this->menu[$i]['name'] = $node->filter('a')->text();
            $this->menu[$i]['price'] = $node->filter('.td_two')->text();
            $this->menu[$i]['weight'] = $node->filter('.td_three')->text();
        });
//echo '<pre>';print_r($this->menu);echo '</pre>';exit(); 
    }


}
