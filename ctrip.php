<?php

require 'vendor/autoload.php';

define("APP_PATH",  realpath(dirname(__FILE__)));
$app = new Yaf_Application(APP_PATH . "/conf/application.ini");
$app->execute("getCity");

function getCity()
{
    $city_json = file_get_contents("/tmp/city.json");
    $city = json_decode($city_json, TRUE);

    $hot = $city['XYZ'];
    $all = [];
    $cityMod  = new CtripModel();
    foreach ($hot as $b) {
        foreach($b as $c)
        {
            $data = explode('|', $c['data']);
            $all['name'] = $c['display'];
            $all['code'] = $data[3];
            $all['remark'] = $data[1];
        $cityMod->addCity($all);

        }
    }

}
